import Airplane
class TimeSlotModel:
     """Represents a single Airplane Schedule."""

     def __init__(self, data):
          """constructor method with parameters."""
          print("file succesfully scanned in")
          self.queue = []
          self.file = data


     def isEmpty(self):
          """check for empty list."""
          return len(self.queue) == []

     def insertPlane(self, plane):
          """to add an Airplane to list."""
          self.queue.append(plane)


     def __str__(self):
         """ToString for queue with AirPlane Objects."""
         count = 0
         while count < len(self.queue):
              id = self.queue[count].planeId
              return id
              count += 1



     def dataToList(self):
         """reads lines from data.txt, and creates Airplane classes based on each Line."""
         with open("data.txt", "r") as filestream:
              with open("answers.txt", "w") as filestreamtwo:
                   for line in filestream:
                        currentline = line.split(",")
                        plane = Airplane.Airplane((currentline[0]) , int(currentline[1]) , int(currentline [2]) , int(currentline [3]))
                        total = str(int(currentline[0]) + int(currentline[1]) + int(currentline [2]) + int(currentline [3])) + "\n"
                        self.insertPlane(plane)
                        filestreamtwo.write(total)


     def latestSubmissionTime(self):
         """returns the latest submissionTime for the planes in queue list"""
         latestTime = 0
         i = 0
         while i < len(self.queue):
             if latestTime < self.queue[i].submissionTime:
                 latestTime = self.queue[i].submissionTime
             i += 1
         return latestTime


     def displayQueue(self):
         """displays queue with time incriments of 1, starting at time 0"""
         time = 0 #represents time starting at 0.
         timeStepList = []
         taxiTime = 0
         #will only go until thelatest submissionTime
         while time <= self.latestSubmissionTime():
             if (time == self.latestSubmissionTime()):
                 print("----The Final Schedule---")
             else:
                 print("queue at time : " + str(time))
             i=0
             #now iterate through the list and only get the planes with sumbission time <= time.
             while i < len(self.queue):
                  if self.queue[i].submissionTime <= time:
                      timeStepList.append(self.queue[i])
                  i += 1
             # here before the first loop ends and before the list gets cleared, sort based on rt and taxi time.
             #first sort than print the loop
             timeStepList.sort(key=lambda x: x.requestedStart, reverse=False)
             k=0
             while k < len(timeStepList):
                if (time == self.latestSubmissionTime()):
                    print("   plane ID: " + timeStepList[k].planeId + "   left at: " + str(taxiTime))
                else:
                    print("   plane ID: " + timeStepList[k].planeId + "   sheduled for: " + str(taxiTime))
                taxiTime += timeStepList[k].requestedDuration
                k += 1
             #clear the timeStepList for next second iteration.
             timeStepList.clear()
             taxiTime=0
             time += 1
