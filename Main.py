import sys
import Airplane
import TimeSlotModel
class Main:

    if __name__ == '__main__':
         """Represents the main for a air plane Schedule"""
         def main():
            """"what runs when main is called"""
            filename = sys.argv[1]
            flightSchedule = TimeSlotModel.TimeSlotModel("filename")
            flightSchedule.dataToList()
            flightSchedule.displayQueue()
    main()
