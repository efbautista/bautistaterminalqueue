import sys
class Airplane(object):
    """Represents a single Airplane"""
    def __init__(self, id, st, rs, rd,):
         """constructor method with plane data parameters."""
         self.planeId = id
         self.submissionTime = st
         self.requestedStart = rs
         self.requestedDuration = rd
